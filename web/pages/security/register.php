<?php
// Include config file
require_once "../../autoloader.php";

// Define VARIABLES AND initialize WITH empty VALUES
$username = $password = $confirm_password = "";
$username_err = $password_err = $confirm_password_err = "";

if ($_SERVER["REQUEST_METHOD"] == "POST") {

    LoginService::Register($_POST);
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Sign Up</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">
    <STYLE TYPE="text/css">
        body{ font: 14px sans-serif; }
        .wrapper{ width: 350px; padding: 20px; }
    </style>
</head>
<body>
    <center>
    <div class="wrapper">
        <h2>Increva-se</h2>

        <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
            <div class="form-GROUP">
                <label>Nome Utilizador</label>
                <INPUT TYPE="text" name="username" class="form-control" VALUE="">
            </div>
            <div class="form-GROUP <?php echo (!empty($password_err)) ? 'has-error' : ''; ?>">
                <label>Password</label>
                <INPUT TYPE="password" name="password" class="form-control" VALUE="">

            </div>
            <div class="form-GROUP <?php echo (!empty($confirm_password_err)) ? 'has-error' : ''; ?>">
                <label>Confirmar Password</label>
                <INPUT TYPE="password" name="confirm_password" class="form-control" VALUE="">

            </div>
            <div class="form-group">
                <INPUT TYPE="submit" class="btn btn-primary" VALUE="Submit">
                <INPUT TYPE="reset" class="btn btn-default" VALUE="Reset">
            </div>
            <p>Already have an account? <a href="login.php">Login aqui</a>.</p>
        </form>
    </div>
                </center>
</body>
</html>
