<?php

// System Libs
require_once __DIR__ . "/System/Database.php";
require_once __DIR__ . "/System/SecurityClass.php";
require_once __DIR__ . "/System/AlertService.php";
require_once __DIR__ . "/System/ValidationForm.php";


// Entity Service
require_once __DIR__ . "/EntityServices/LoginService.php";
require_once __DIR__ . "/EntityServices/ProdutoService.php";


// web/lib/System/DatabaseService.php
// web/lib/System/SecurityClass.php
