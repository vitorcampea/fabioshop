<?php require_once "../../autoloader.php"; ?>
<?php require_once "../../layout/header.php"; ?>

<?php

// Form required fields


if ($_SERVER["REQUEST_METHOD"] == "POST") {
    LoginService::Reset($_POST);
}

?>



<div class="container">


    <h2>Reset Password</h2>
    <p>Please fill out this form to reset your password.</p>
    <form class="form-horizontal" action="<?php echo $_SERVER["PHP_SELF"]; ?>" method="post">

        <div class="form-group">
            <label class="col-md-4 control-label" for="textinput">password</label>
            <div class="col-md-4">
                <input id="password" name="password" type="text" placeholder="placeholder"
                    class="form-control input-md">
                <span class="help-block">password</span>
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-4 control-label" for="textinput">confirme password</label>
            <div class="col-md-4">
                <input id="confirm_password" name="confirm_password" type="text" placeholder="placeholder"
                    class="form-control input-md">
                <span class="help-block">confirme password</span>
            </div>
        </div>

        <!-- Button -->
        <div class="form-group">
        <div class="col-md-4">
            <button id="singlebutton" type="submit" class="btn btn-primary">Gravar</button>
        </div>
        </div>

    </form>

</div>


    <?php require_once "../../layout/footer.php"; ?>
