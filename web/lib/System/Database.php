<?php

class Database {

    private static $db;
    private $connection;

    public function __construct($IP, $USER, $PASSWORD, $DB, $PORT) {

        /* Attempt to connect to MySQL database */
        self::$db = mysqli_connect($IP, $USER, $PASSWORD, $DB, $PORT);

        // CHECK connection
        if (!self::$db) {
            echo "Error: Unable to connect to MySQL." . PHP_EOL;
            echo "Debugging errno: " . mysqli_connect_errno() . PHP_EOL;
            echo "Debugging error: " . mysqli_connect_error() . PHP_EOL;
            exit;
        }

    }

    public static function getConnection() {
        return self::$db;
    }
}

?>
