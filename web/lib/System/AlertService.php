<?php

class AlertService
{
    private static $alerts = [];

    public static function Add($message)
    {
        if(!is_array(self::$alerts)) {
            self::$alerts = [];
        }

        self::$alerts[] = $message;
    }

    public static function Print()
    {
        foreach (self::$alerts as $key => $value) {
            echo "<div class=\"alert alert-warning\" role=\"alert\">
                $value
            </div>";
        }
    }

    public static function Error($message = "Não foi possivel")
    {
        echo "<div class=\"alert alert-danger\" role=\"alert\">
            $message
        </div>";
    }

    public static function Success($message = "Sucesso")
    {
        echo "<div class=\"alert alert-success\" role=\"alert\">
            $message
        </div>";
    }


}

?>
