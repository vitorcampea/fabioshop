<?php require_once "../../autoloader.php"; ?>

<?php

// Initialize the session
session_start();

// Unset all of the session variables
$_SESSION = array();

// Destroy the session.
session_destroy();

// Redirect to login page
header("Location: " . PROJECT_ENDPOINT . "/web/pages/security/login.php");
?>
