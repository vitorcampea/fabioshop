<?php

class LoginService
{
    public static $requiredFields = [
        "product_name",
        "price"
    ];

    public static function Login($username, $password)
    {
        if(empty ($username)) {
            AlertService::Error("username está vazio");
        }

        if(empty ($password)) {
            AlertService::Error("password está vazio");
        }

        $query = "SELECT
                    id,
                    username,
                    `name`,
                    `password`,
                    created_at,
                    email
                FROM
                    users
                WHERE
                    username LIKE '$username'
                    AND PASSWORD LIKE MD5( '$password' )
                limit 1";

        ValidationForm::Debug($query);

        //ler da db se o user existe
        $result = mysqli_query(Database::getConnection(), $query);

        if($result == false) {
            AlertService::Error("Error");
        }

        $user = [];
        $user['id'] = null;
        $user['username'] = null;
        $user['name'] = null;
        $user['email'] = null;

        if (mysqli_num_rows($result) > 0) {
            while($row = mysqli_fetch_assoc($result)) {

               $user['id'] = $row["id"];
               $user['username'] = $row["username"];
               $user['name'] = $row["name"];
               $user['email'] = $row["email"];

               $usernameFromDatabase = $row["username"];
            break;
            }
         }

        if($user['username'] === $username) {
            $_SESSION["loggedin"] = true; //sair

            $_SESSION["id"] = $user['id'];
            $_SESSION["username"] = $user['username'];
            $_SESSION["name"] = $user['name'];
            $_SESSION["email"] = $user['email'];

            header("location: " . PROJECT_ENDPOINT . "/web/pages/welcome.php");
            die();
        }
        else {
            AlertService::Error("Login invalido");
        }
    }

    public static function Register($POST)
    {
        ValidationForm::Validate($_POST,
            [
                "username",
                "password",
                "confirm_password"
            ]
        );

        if($_POST['password'] !== $_POST['confirm_password'])
        {
            AlertService::Error("Password não são iguais");
        }

        $columns = [
            "`" . "username" . "`",
            "`" . "password" . "`"
        ];

        $values = [
            "'" . $_POST['username']. "'",
            "'" . md5($_POST['password']). "'"
        ];


        $query = "INSERT INTO `users` (" . implode(",", $columns) . ") VALUES (" . implode(",", $values) . ")";

        ValidationForm::Debug($query);

        if (!Database::getConnection()->query($query)) {
            AlertService::Error();
        }else {
            AlertService::Success();
        }
    }

    public static function Reset($POST)
    {
        ValidationForm::Validate($_POST,
            [
                "password",
                "confirm_password"
            ]
        );

        if($_POST['password'] !== $_POST['confirm_password'])
        {
            AlertService::Error("Password não são iguais");
        }

        $password = md5($_POST['password']);
        $id = $_SESSION["id"];

        $query = "UPDATE `users` SET `password` = '$password' WHERE `id` = '$id' limit 1";

        ValidationForm::Debug($query);

        if (!Database::getConnection()->query($query)) {
            AlertService::Error();
        }else {
            AlertService::Success();
        }
    }

    public static function Logout()
    {

    }
}

?>
