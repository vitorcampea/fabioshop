/*
 Navicat MySQL Data Transfer

 Source Server         : camisolas
 Source Server Type    : MySQL
 Source Server Version : 50726
 Source Host           : localhost:33062
 Source Schema         : futfanaticos

 Target Server Type    : MySQL
 Target Server Version : 50726
 File Encoding         : 65001

 Date: 10/05/2020 18:46:23
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for produtos
-- ----------------------------
DROP TABLE IF EXISTS `produtos`;
CREATE TABLE `produtos`  (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `product` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `price` float(255, 0) NULL DEFAULT NULL,
  `product_image` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 24 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of produtos
-- ----------------------------
INSERT INTO `produtos` VALUES (13, 'asd', NULL, NULL, '2020-05-10 16:58:05');
INSERT INTO `produtos` VALUES (14, '', NULL, NULL, '2020-05-10 16:58:07');
INSERT INTO `produtos` VALUES (15, 'asdsad', NULL, NULL, '2020-05-10 16:58:27');
INSERT INTO `produtos` VALUES (16, 'dfgdfg', NULL, NULL, '2020-05-10 17:01:25');
INSERT INTO `produtos` VALUES (17, 'dfgdfg', NULL, NULL, '2020-05-10 17:01:34');
INSERT INTO `produtos` VALUES (18, 'dfgd', NULL, NULL, '2020-05-10 17:01:37');
INSERT INTO `produtos` VALUES (19, 'sdfs', NULL, NULL, '2020-05-10 17:02:48');
INSERT INTO `produtos` VALUES (20, 'asdad', NULL, NULL, '2020-05-10 17:03:23');
INSERT INTO `produtos` VALUES (21, 'vitor', 123, NULL, '2020-05-10 17:06:27');
INSERT INTO `produtos` VALUES (22, 'asdasd', 12321, NULL, '2020-05-10 17:09:49');
INSERT INTO `produtos` VALUES (23, 'asdsad', 123123, '7ece8492b867a462.txt', '2020-05-10 17:12:40');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `created_at` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0),
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `username`(`username`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (1, 'jorge', NULL, '$2y$10$HX5v.XxckCAGJOVsdtuj4.1Gw8Uni4Kew6Zg5/KtsddYA5NkqT2xC', '2019-12-04 15:15:04', NULL);
INSERT INTO `users` VALUES (2, 'vitor', NULL, '4124bc0a9335c27f086f24ba207a4912', '2020-05-10 17:47:30', NULL);
INSERT INTO `users` VALUES (3, 'ssaddas', NULL, 'asdsad', '2020-05-10 18:18:18', NULL);
INSERT INTO `users` VALUES (4, '', NULL, 'asd', '2020-05-10 18:23:51', NULL);
INSERT INTO `users` VALUES (5, 'fabio', NULL, '827ccb0eea8a706c4c34a16891f84e7b', '2020-05-10 18:25:31', NULL);

SET FOREIGN_KEY_CHECKS = 1;
