<?php

class ProductService
{
    public static $requiredFields = [
        "product_name",
        "price"
    ];

    public static function Add($form)
    {
        ValidationForm::Validate($_POST, self::$requiredFields);

        $columns = [
            "`" . "product" . "`",
            "`" . "price" . "`",
            "`" . "product_image" . "`"
        ];

        $values = [
            "'" . $_POST['product']. "'",
            "'" . $_POST['price']. "'",
            "'" . $_POST['product_image']. "'",

        ];


        $query = "INSERT INTO `produtos` (" . implode(",", $columns) . ") VALUES (" . implode(",", $values) . ")";

        ValidationForm::Debug($query);

        if (!Database::getConnection()->query($query)) {
            AlertService::Error();
        }else {
            AlertService::Success();
        }
    }

    public static function Update()
    {

    }

    public static function Delete()
    {

    }

    public static function List()
    {

        $query =" SELECT
            produtos.Id,
            produtos.product,
            produtos.price,
            produtos.product_image,
            produtos.created_at
        FROM
            produtos";

        $result = mysqli_query(Database::getConnection(), $query);

        $productList = [];
        if (mysqli_num_rows($result) > 0) {
            while($row = mysqli_fetch_assoc($result))
            {
                $productList[] = [
                    "id" => $row["Id"],
                    "product" => $row["product"],
                    "price" => $row["price"],
                    "product_image" => $row["product_image"],
                    "created_at" => $row["created_at"]
                ];

            }
         }

        return $productList;
    }
}

?>
