<?php require_once "../../autoloader.php"; ?>
<?php require_once "../../layout/header.php"; ?>

<?php

// Form required fields


if ($_SERVER["REQUEST_METHOD"] == "POST") {
    ProductService::Add("asd", $_POST);
}

?>



<div class="container">

  <form class="form-horizontal" action="<?php echo $_SERVER["PHP_SELF"]; ?>" method="post">
    <!-- Form Name -->
    <legend>Produto - Camisola</legend>

    <!-- Text input-->
    <div class="form-group">
      <label class="col-md-4 control-label" for="textinput">Nome</label>
      <div class="col-md-4">
        <input id="product" name="product" type="text" placeholder="placeholder" class="form-control input-md">
        <span class="help-block">Nome do produto</span>
      </div>
    </div>

    <div class="form-group">
      <label class="col-md-4 control-label" for="textinput">Preço</label>
      <div class="col-md-4">
        <input id="price" name="price" type="number" placeholder="0" class="form-control input-md">

      </div>
    </div>

    <div class="form-group">
      <label class="col-md-4 control-label" for="textinput">Imagem</label>
      <div class="col-md-4">
        <input id="product_image" name="product_image" type="file" class="custom-file-input" id="inputGroupFile01"
          aria-describedby="inputGroupFileAddon01">

      </div>
    </div>

    <!-- Button -->
    <div class="form-group">
      <div class="col-md-4">
        <button id="singlebutton" type="submit" class="btn btn-primary">Gravar</button>
      </div>
    </div>

  </form>
</div>



  <?php require_once "../../layout/footer.php"; ?>
