<html lang="en">

  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Navbar Template for Bootstrap</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  </head>

  <body>

    <div class="container">

      <!-- Static navbar -->
      <nav class="navbar navbar-default">
        <div class="container-fluid">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
              aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Fabio Mega Project</a>
          </div>
          <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
              <li><a href="<?php echo PROJECT_ENDPOINT; ?>/web/pages/welcome.php">Home</a></li>
              <li><a href="#Clubes">Clubes</a></li>
              <li><a href="#Promoções">Promoções</a></li>
              <li><a href="<?php echo PROJECT_ENDPOINT; ?>/web/pages/produtos/create_produto.php">Criar Produto</a></li>

            </ul>
            <ul class="nav navbar-nav navbar-right">
              <li><a><?php echo htmlspecialchars($_SESSION["username"]) ?></a>  </li>
              <li><a href="<?php echo PROJECT_ENDPOINT; ?>/web/pages/security/reset_password.php">Reset Password</a>  </li>
              <li><a href="<?php echo PROJECT_ENDPOINT; ?>/web/pages/security/logout.php">Logout</a></li>
            </ul>
          </div>
          <!--/.nav-collapse -->
        </div>
        <!--/.container-fluid -->
      </nav>

    </div> <!-- /container -->

    <div class="container">
