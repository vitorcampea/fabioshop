<?php

class ValidationForm
{
    public static function Debug($data)
    {
        if(DEBUG === true) {
            echo '<pre>' . print_r($data, true) . '</pre>';
        }
    }


    public static function Validate($postForm, $requiredFields) {

        ValidationForm::Debug($postForm);

        foreach ($postForm as $key => $value) {
            //echo "Field ".htmlspecialchars($key)." is ".htmlspecialchars($value)."<br>";

            if(in_array($key, $requiredFields) ) {

                if(empty ($value)) {
                    AlertService::add("$key está vazio");
                }
            }
        }

        AlertService::Print();
    }
}

?>
