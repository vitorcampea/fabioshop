<?php require_once "../autoloader.php"; ?>
<?php require_once "../layout/header.php"; ?>

<?php

$productlist = ProductService::List();

echo "<br>";

echo '<div class=\"carousel-inner\">';
foreach ($productlist as $key => $produto) {
  echo "{$produto['product']} - $IMAGE_DIR/{$produto['product_image']}";
?>
    <div class="item active">
      <img src="<?php echo $IMAGE_DIR. "/" .$produto['product_image']; ?>" alt="Barcelona" style="width:55%;">
    </div>

<?php
}
echo '</div>';
?>



<div class="container">
    <h1>Camisolas</h1>

 <div id="myCarousel" class="carousel slide" data-ride="carousel">
          <!-- Indicators -->
          <center>
            <ol class="carousel-indicators">
              <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
              <li data-target="#myCarousel" data-slide-to="1"></li>
              <li data-target="#myCarousel" data-slide-to="2"></li>
              <li data-target="#myCarousel" data-slide-to="3"></li>
              <li data-target="#myCarousel" data-slide-to="4"></li>
              <li data-target="#myCarousel" data-slide-to="5"></li>
            </ol>
          </center>

          <!-- Wrapper for slides -->
          <div class="carousel-inner">
            <div class="item active">
              <img src="<?php echo $IMAGE_DIR; ?>/barca.jpg" alt="Barcelona" style="width:55%;">
            </div>

            <div class="item">
              <img src="<?php echo $IMAGE_DIR; ?>/real.jpg" alt="Real Madrid" style="width:55%;">
            </div>

            <div class="item">
              <img src="<?php echo $IMAGE_DIR; ?>/porto.jpg" alt="FCPorto" style="width:55%;">
            </div>

            <div class="item">
              <img src="<?php echo $IMAGE_DIR; ?>/wolves.jpg" alt="Wolverhampton" style="width:55%;">
            </div>

            <div class="item">
              <img src="<?php echo $IMAGE_DIR; ?>/liverpool.jpg" alt="Liverpool" style="width:55%;">
            </div>


          </div>
          <!-- Left and right controls -->
          <a class="left carousel-control" href="#myCarousel" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left"></span>
            <span class="sr-only">Atras</span>
          </a>
          <a class="right carousel-control" href="#myCarousel" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right"></span>
            <span class="sr-only">Frente</span>
          </a>
        </div>
        </div>

<?php include "../layout/footer.php"; ?>
